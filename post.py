import requests
# import convert
from bs4 import BeautifulSoup as bs
import jalali
from uuid import uuid1
from time import time
from downloader import download_all

persian_nubmer = {
    '۰': '0',
    '۱': '1',
    '۲': '2',
    '۳': '3',
    '۴': '4',
    '۵': '5',
    '۶': '6',
    '۷': '7',
    '۸': '8',
    '۹': '9',
    '/': '-',
}


def get_post(url):
    '''
    url is something like "https://shirazlug.ir/s0/"
    '''
    html = requests.get(url).content
    soup = bs(html, 'html.parser')

    title = soup.select(".new")[0].text.strip()
    soup = soup.select("#reports1")[0]
    writer = soup.select('span > span')[1].text.split()[1].strip()
    date = soup.select('span > span')[3].text.split()[1].strip()
    jDate = ""
    for i, c in enumerate(date):
        jDate += persian_nubmer[c]
    jDate = "13" + jDate
    gDate = jalali.Persian(jDate).gregorian_datetime()

    img_tags = soup.select("img")
    images = [img["src"] for img in img_tags]
    images = [(img, str(uuid1()) + str(time()) + "." + img.split(".")[-1])
              for img in images]
    for i in range(len(img_tags)):
        print("src", img_tags[i]['src'])
        img_tags[i]['src'] = "img/" + images[i][1]
        parrent = img_tags[i].parent
        if 'href' in parrent.attrs:
            parrent['href'] = "img/" + images[i][1]

    post_body = soup.select(".twitter-share ~ *")
    post_body = "\n".join([str(tag) for tag in post_body])

    return (title, writer, date, jDate, gDate, post_body, images)


if __name__ == "__main__":
    all_images = []
    for i in range(0, 81):
        print("########")
        print("post", i)
        try:
            post = get_post("https://shirazlug.ir/s" + str(i))
            all_images.extend(post[6])
            print("title: ", post[0])
            print("writer: ", post[1])
            print("date: ", post[2])
            print("jDate: ", post[3])
            print("gDate: ", post[4])
            print("post_body: ", post[5])
            print("########")
        except Exception as e:
            print(e)
            print("post", i, "damaged")

    print(all_images)
    download_all(all_images, "static/img")
