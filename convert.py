#!/usr/bin/env python3

import pypandoc
import html2text
import post
import os
import downloader


def pandoc_exists():
    try:
        vers = pypandoc.get_pandoc_version()
        print("Pandoc Found \n",
              "    the version is %s " % vers)
    except:
        print("pandoc not Found!")
        print("installing pandoc")
        pypandoc.download_pandoc()


def make_dir(dirname="Posts_md"):
    """
    given an Absolute path, Creates it after checking if it doesn't exist
    """
    if not os.path.exists(dirname):
        os.makedirs(dirname, mode=0o777)
        # TODO: setting a more secure permission
    else:
        print("Directory %s already exists" % dirname)
        return


def convert2md_pandoc(post_body):
    '''
    Converts the html input to standard markdown output.
    '''
    print("converting to Markdown")
    # arguments passed to convert_text are :
    # 1. input string,
    # 2. output format,
    # 3. input format (for more readability)
    output = pypandoc.convert_text(post_body, 'markdown_github', format='html')
    return output


def convert2md(post_body):
    return html2text.html2text(post_body)


def write_md(fname, input):
    with open(fname, 'w') as outfile:
        outfile.write(input)


def add_title(st, title, date):
    '''
    adding to the head of any string given
    title, date, category
    '''
    head_raw = '''---
title: "{}"
date: "{}"
categories:
    - "sessions"
---
    '''
    head = head_raw.format(title, date)
    return head + st


if __name__ == '__main__':
    # switching to where the content is saved
    make_dir("Posts_md")
    os.chdir("Posts_md")

    pandoc_exists()

    all_images = []
    for ssn in range(0, 81):
        try:
            print("########")
            print("post", ssn)

            print("getting posts")
            cur_post = post.get_post("https://shirazlug.ir/s" + str(ssn))
            all_images.extend(cur_post[6])
            print("Writing session \n%s to File " % cur_post[0])
            # TODO: adding the title to the files
            # TODO: possibly extracting all the images & saving elsewhere

            markdown_output = convert2md(cur_post[5])
            output = add_title(markdown_output, cur_post[0], cur_post[3])
            write_md("session" + str(ssn) + ".md", output)
        except Exception as err:
            print(err)
            print("post", ssn, "damaged")


    downloader.download_all(all_images, "static/img")
