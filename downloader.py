from sys import argv
import os


def download_one(link, name="", path=""):
    save_dir_option = ""
    if (path != "" and not os.path.isdir(path)):
        os.system("mkdir -p " + path)
    if (path != ""):
        save_dir_option = "-d " + path
    if name == "":
        cmd = 'aria2c --max-connection-per-server=16'
        print('$$> Download started:')
        os.system(cmd + " " + link + save_dir_option)
        print('$$> Download finished :)')
    else:
        cmd = 'aria2c --max-connection-per-server=16'
        print('$$> Download started:')
        os.system(cmd + " " + link + " -o " + name + " " + save_dir_option)
        print('$$> Download finished :)')


def download_all(links, path=""):
    for link in links:
        if type(link) == tuple:
            download_one(link[0], link[1], path)
        else:
            download_one(link, "", path)
